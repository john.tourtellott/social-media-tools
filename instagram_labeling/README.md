# Setup
Have the latest [girder](http://girder.readthedocs.io/en/latest/installation.html#install-from-git-repository) up and running  
Execute `girder-install plugin -s {path_to_this_directory}` within the girder virtualenv  
Execute `girder-install web --dev --plugins instagram_labeling`  
Run `girder-server`  
Navigate to `localhost:8080` and create a user and enable this instagram_labeling plugin  
Stop and restart the server  
Navigate to `localhost:8080`  
