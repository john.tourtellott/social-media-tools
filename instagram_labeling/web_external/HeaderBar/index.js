import React, { PureComponent } from 'react';
import { logout } from 'girder/auth';
import events from 'girder/events';
import { getApiRoot } from 'girder/rest';

import './style.styl';

class HeaderBar extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            showClipExplorer: false
        };
    }

    render() {
        let user = this.props.user;
        return <div className={['v-header-wrapper', this.props.className].join(' ')}>
            <div className='clip-name'>{this.props.selectedFolder ? this.props.selectedFolder.name : null}</div>
            <div className='v-current-user-wrapper toolbutton'>
                {user
                    ? <div className='v-user-link-wrapper'>
                        <a className='v-user-link' data-toggle='dropdown' data-target='#g-user-action-menu'>
                            {user.get('firstName')} {user.get('lastName')}
                            <i className='icon-down-open'></i>
                        </a>
                        <div id='g-user-action-menu' className='dropdown'>
                            <ul className='dropdown-menu' role='menu'>
                                <li role='presentation'>
                                    <a className='g-logout' onClick={() => logout()}>
                                        <i className='icon-logout'></i>
                                        Log out
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    : <div className='v-login-link-wrapper'>
                        <a className='g-register' onClick={(e) =>
                            events.trigger('g:registerUi')}>Register</a>
                        or
                        <a className='g-login' onClick={(e) => events.trigger('g:loginUi')}>
                            Log In
                            <i className='icon-login'></i>
                        </a>
                    </div>
                }
            </div>
        </div>;
    }

    handleClipExplorerTryClose() {
        this.setState({ showClipExplorer: false });
    }

    itemSelected(folder, item) {
        // this.setState({ selectedFolder: folder });
        this.props.dispatch({
            type: SELECTED_FOLDER_CHANGE,
            folder
        });
        this.props.dispatch({
            type: SELECTED_ITEM_CHANGE,
            payload: item
        });
        this.props.dispatch(loadAnnotation(item));
        this.handleClipExplorerTryClose();
    }
}

export default HeaderBar;
