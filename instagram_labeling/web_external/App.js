import React from 'react';
import App from 'girder/views/App';
import { render } from 'react-dom';

import AppContainer from './AppContainer';
import template from './templates/layout.pug';

import './stylesheets/layout.styl';

const thisApp = App.extend({
    render() {
        this.$el.html(template());

        render(
            <AppContainer />,
            document.getElementById('g-app-body-container')
        );

        return this;
    }

});
export default thisApp;
