import React, { PureComponent } from 'react';
import { HashRouter as Router, Route, Link, Redirect, Switch } from 'react-router-dom'
import events from 'girder/events';
import { getCurrentUser } from 'girder/auth';

import MainView from '../MainView';
import HeaderBar from '../HeaderBar';

import './style.styl';

class AppContainer extends PureComponent {
    constructor(props) {
        super(props);
        // This App is small and probaby will remain small, so all application state is kept here and a dummy redux store
        this.state = {
            user: getCurrentUser(),
        };
    }

    componentDidMount() {
        events.on('g:login', () => {
            this.setState({ user: getCurrentUser() });
        });
    }
    render() {
        return [<HeaderBar
            className='v-header'
            key='header-bar'
            user={this.state.user}
        />,
        <Router key='index-view'>
            <Switch>
                <Route exact path="/">
                    <Redirect to="/post" />
                </Route>
                <Route path='/post/:postId?' className='main' component={MainView} />
            </Switch>
        </Router>];
    }
}

export default AppContainer;
