from girder.models.model_base import Model


class Post(Model):

    def initialize(self):
        self.name = 'post'
        self.ensureIndex('original.id')
        self.ensureIndex('_id')

    def validate(self, model):
        return model

    def getByPostId(self, postId):
        posts = list(Post().find({'original.id': postId}).limit(1))
        if len(posts) == 0:
            return None
        return posts[0]
