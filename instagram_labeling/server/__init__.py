#!/usr/bin/env python
# -*- coding: utf-8 -*-

from girder.utility import setting_utilities

import loader

def load(info):
    loader.load(info)

@setting_utilities.validator({
    'instagram_labelling.image_path'
})
def validatePath(doc):
    pass
