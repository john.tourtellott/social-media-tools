from __future__ import print_function
import argparse
import json
import os
import string
import sys

#from filter_posts import InstagramFilter
from filter_alcohol_posts import InstagramAlcoholPostFilter

# List of corresponding input & output files
# Hard-coded because Birdsong naming is inconsistent
states_table = [
    ('Arizona15aDaysInstagraPostsArizona15aDays.json',         'arizona.json',     'AZ', 'Arizona'),
    ('California7DaysInstagraPostsCalifornia7Days.json',       'california.json',  'CA', 'California'),
    ('Illionois215DaysInstagraPostsIllionois215Days.json',     'illinois.json',    'IL', 'Illinois'),
    ('Nevada215DaysInstagraPostsNevada215Days.json',           'nevada.json',      'NV', 'Nevada'),
    ('NYS215aDaysInstagraPostsNYS215aDays.json',               'newyork.json',     'NY', 'New York'),
    ('NorthDakota215DaysInstagraPostsNorthDakota215Days.json', 'northdakota.json', 'ND', 'North Dakota'),
    ('Oaklahoma215DaysInstagraPostsOaklahoma215Days.json',     'oklahoma.json',    'OK', 'Oklahoma'),
    ('Oregon74DaysInstagraPostsOregon74Days.json',             'oregon.json',      'OR', 'Oregon'),
    ('SouthDakota215DaysInstagraPostsSouthDakota215Days.json', 'southdakota.json', 'SD', 'South Dakota'),
    ('Texas215DaysInstagraPostsTexas215Days.json',             'texas.json',       'TX', 'Texas'),
    ('Utah215DaysInstagraPostsUtah215Days.json',               'utah.json',        'UT', 'Utah'),
    ('Vermont215DaysInstagraPostsVermont215Days.json',         'vermont.json',     'VT', 'Vermont')
]


if __name__ == '__main__':
    description = 'Runs filter_posts for states directory'
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', required=True, \
        help='Directory for input & output files')
    parser.add_argument('-e', '--match_full_words', action='store_true',
        help='match full words instead of partial')
    parser.add_argument('-i', '--index', type=int, \
        help='only process one state, by index')
    options = parser.parse_args()

    os.chdir(options.directory)
    iap_filter = InstagramAlcoholPostFilter(
        match_full_words=options.match_full_words)
    for i,(infile,outfile,tag,state) in enumerate(states_table):
        if options.index and i != options.index:
            continue
        print('Processing {}'.format(infile))
        iap_filter.filter_posts(
            infile,
            output_json_file=outfile,
            state=state)
