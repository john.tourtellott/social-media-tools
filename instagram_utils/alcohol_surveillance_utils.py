
alcohol_keywords = [
    'absolut ',       # vodka
    'aguila',        # beer
    'alcohol',
    'alesmith',      # brewery
    'amstel',        # beer
    'baccardi',
    'ballantine',
    'beer',
    'beerlove',
    'beermakesme',
    'belvedere',
    'booze',
    'bourbon',
    'brewery',
    'brewing'
    'carlsberg',
    'cerveza',
    'champagne',
    'chandon',
    'chivas',
    'ciroc',
    'cocktail',
    'cognac',
    'cointreau',
    'conchaytoro',
    'corona',        # beer
    'coronoa',       # beer
    'crookedstave',  # vodka
    'crystalhead',
    'descutes',
    'dessertwine',
    'disaronno',
    'donjulio',      # tequila
    'dosequis',      # beer
    'drydock',
    'dusse',         # cognac
    'elysianbrew',
    'epicbeer',
    'epicbrew',
    'frescobaldi',   # wine
    ' gin ',
    'ginandtonic',
    'gintonic',
    'gooseisland',   # beer
    'greygoose',     # vodka
    'guinness',      # beer
    'heineken',      # beer
    'hennessy',      # cognac
    'ikibeer',
    'indio',         # beer
    'jackdaniel',    # whiskey
    'jagermeister',
    'jagermaister',
    'jagerbomb',
    'jelloshots',
    'johnniewalker',  # whiskey
    'lagunitas',
    'liquer',
    'liquor',
    'longislandicedtea',  # cocktail
    'maliburum',
    'martell',        # cognac manufacturer
    'martini',
    'moet',
    'mojito',
    'newbelgium',
    'pabst',
    'pabstblueribbon',
    'penfolds',
    'perrierjoet',
    ' pub ',
    ' pubs ',
    'redwine',
    'reedsginger',
    'relaxwine',
    'rhum',
    ' rum ',
    'rumandcoke',
    'sambuca',
    'scotch',
    'scotchlove',
    'scothmakesme',
    'skyyvodka',
    'smirnoff',
    'sparklingwine',
    'stolichnaya',  # vodka,
    'svedka',
    'tequila',
    'vermouth',
    'veuvecliquot',
    'vodka',
    'vodkalove',
    'vodkamakesme',
    'whiskey',
    'whiskeylove',
    'whiskeymakesme',
    'whisky',
    'whiskylove',
    'whiskymakesme',
    'whitewine',
    ' wine',
    'winelove',
    'winemakesme',
    'winery',
    'vineyard',
]

other_keywords = [

    # alcoholic slang words
    ' cheers ', 'drank', 'drink', 'drinkup', 'drunk', 'omg', 'stoned',
    'sloshed', 'wasted',

    # keywords and/or phrases helpful for hard-negative cases
    'available', 'admission', ' come ', 'come join', 'come celebrate',
    'complimentary', ' cover ', ' charge ', 'customer', ' dj ', 'discount',
    'email', 'happyhour', ' join ', ' join us ', 'menu', 'maximum', 'minimum',
    'offer', 'open bar', 'phone', 'photo', ' pic ', 'performance', 'purchase',
    'reservation', 'refill', 'repost', 'rsvp', 'selection', 'shop', 'store',
    'special', 'support', 'ticket', 'tkts', 'with us',

    # personal pronouns
    ' i ', ' im ', ' ive ', ' me ', ' my ', ' we ', ' our ', ' us '
]

alcohol_emoji_dict = {

    u'\U0001F37E': 'bottle_with_popping_cork',
    u'\U0001F377': 'wine_glass',
    u'\U0001F378': 'cocktail_glass',
    u'\U0001F379': 'tropical_drink',
    u'\U0001F37A': 'beer_mug',
    u'\U0001F37B': 'clinking_beer_mugs',
    u'\U0001F942': 'clinking_glasses',
    u'\U0001F943': 'tumbler_glass',
}
