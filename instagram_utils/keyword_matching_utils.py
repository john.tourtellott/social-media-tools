import ahocorasick as ahc


def make_aho_keyword_automaton(keyword_list):
    """
    Builds the aho corasick automaton from a list of keywords
    """

    kw_automaton = ahc.Automaton()

    for w in keyword_list:
        kw_automaton.add_word(w, (w, w))

    kw_automaton.make_automaton()

    return kw_automaton


def has_any_keyword(input_text, kw_automaton):
    """
    Checks if the input text as any of keywords in the automaton
    """

    for end_index, (cat, keyw) in kw_automaton.iter(input_text):
        return True

    return False


def find_keywords(input_text, kw_automaton):
    """
    Finds all keywords in the automaton that are present in the input text
    """

    found_keywords = []
    for end_index, (cat, keyw) in kw_automaton.iter(input_text):
        found_keywords.append(keyw)
    return found_keywords