
# Command line script to dump "labeled" data store in mongo db
    mongoexport --db girder --collection post --query '{"result": {$in: [true,false]} }' --csv --out data.csv --fields '_id,original.user.username,original.user.id,original.caption.text,original.tags,original.images.standard_resolution.url,resultUser,result,imageResult'



# Example of Instagram record json format

john@turtleland2:~$ mongo
MongoDB shell version: 2.4.9
connecting to: test
> use girder
switched to db girder
> db.post.findOne()
{
	"_id" : ObjectId("5a905b6ead3cd748dc8f5342"),
	"resultUser" : "deepak.chittajallu",
	"result" : true,
	"original" : {
		"userHasLiked" : false,

		"likes" : {
			"total" : 21
		},
		"createdTime" : "Jan 1, 2018 2:59:54 AM",
		"tags" : [
			"endof2017"
		],
		"comments" : {
			"total" : 3
		},
		"filter" : "Juno",
		"caption" : {
			"createdTime" : "Jan 1, 2018 2:59:54 AM",
			"text" : "The #endof2017 doesn't call for champagne. It requires whiskey 🥃",
			"from" : {
				"username" : "katedorlan",
				"comments" : 0,
				"likes" : 0,
				"privateRef" : false,
				"fullName" : "Kate Dorlan",
				"id" : NumberLong("6067364131"),
				"profilePictureUrl" : "https://scontent.cdninstagram.com/t51.2885-19/s150x150/24177927_197473107487570_974319618929721344_n.jpg"
			},
			"id" : NumberLong("17916119251036825")
		},
		"link" : "https://www.instagram.com/p/BdZBZbOHVV_/",
		"location" : {
			"latitude" : 40.7142,
			"id" : 212988663,
			"longitude" : -74.0064,
			"name" : "New York, New York"
		},to_asc
		"images" : {
			"low_resolution" : {
				"url" : "https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/25024707_160250334740976_1771125675420286976_n.jpg",
				"width" : 320,
				"height" : 320
			},
			"thumbnail" : {
				"url" : "https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/25024707_160250334740976_1771125675420286976_n.jpg",
				"width" : 150,
				"height" : 150
			},
			"standard_resolution" : {
				"url" : "https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/25024707_160250334740976_1771125675420286976_n.jpg",
				"width" : 640,
				"height" : 640
			}
		},
		"type" : "image",
		"id" : "1682382081060853119_6067364131",
		"user" : {
			"username" : "katedorlan",
			"comments" : 0,
			"likes" : 0,
			"privateRef" : false,
			"fullName" : "Kate Dorlan",
			"id" : NumberLong("6067364131"),
			"profilePictureUrl" : "https://scontent.cdninstagram.com/t51.2885-19/s150x150/24177927_197473107487570_974319618929721344_n.jpg"
		}
	},
	"imageResult" : true
}
