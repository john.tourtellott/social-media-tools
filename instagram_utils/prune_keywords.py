"""Removes any keyword that contains another keyword
"""
from __future__ import print_function
import argparse
import os
import sys

class KeywordFilter:
    def __init__(self, options):
        self.options = options
        self.word_table = dict() # store list of words by size

    def load_keywords(self):
        self.word_table.clear()
        count = 0
        with open(self.options.input_filename) as inp:
            while True:
                line = inp.readline()
                if not line:
                    break

                count += 1
                word = line.strip()
                l = len(word)
                word_list = self.word_table.get(l, [])
                word_list.append(word)
                self.word_table[l] = word_list
        print('Input word_count {}'.format(count))

    def prune_data(self):
        '''Generates list with non-redundant words

        '''
        output_list = list()
        items = sorted(self.word_table.items())
        min_len = items[0][0]
        max_len = items[-1][0]
        print('min_len {}, max_len {}'.format(min_len, max_len))
        for l,word_list in items:
            print('input words of length {}: {}'.format(l, len(word_list)))
            # Search for matches in larger-size words
            for m in range(l+1,max_len+1,1):
                self._prune_list(l, m)

    def write_output(self):
        ''''''
        output_list = list()
        for word_list in self.word_table.values():
            output_list += word_list
        print('Output word_count {}'.format(len(output_list)))

        with open(self.options.output_filename, 'w') as out:
            output_list = sorted(output_list)
            output_string = '\n'.join(output_list)
            out.write(output_string)
            out.write('\n')
            print('Wrote {}'.format(self.options.output_filename))


    def _prune_list(self, lower_length, upper_length):
        '''Updates one entry in the word table

        '''
        lower_list = self.word_table.get(lower_length)
        input_list = self.word_table.get(upper_length)
        output_list = list()
        if not input_list:
            return

        count = 0
        for input_word in input_list:
            keep = True
            for lower_word in lower_list:
                if input_word.find(lower_word) >= 0:
                    #print('Drop {} which matches {}'.format(input_word, lower_word))
                    keep = False
                    count += 1
                    break
            if keep:
                output_list.append(input_word)

        if count > 0:
            self.word_table[upper_length] = output_list


if __name__ == '__main__':
    description = 'Remove words from list that are supersets of other words'
    parser = argparse.ArgumentParser(description=description)

    # Default input file:
    source_dir = os.path.abspath(os.path.dirname(__file__))
    default_input_file = os.path.join(source_dir, 'alcohol_keywords.txt')
    parser.add_argument('-i', '--input_filename', default=default_input_file, \
        help='Input file')
    parser.add_argument('-o', '--output_filename', help='Output filename (json)')
    options = parser.parse_args()

    print(options)
    kfilter = KeywordFilter(options)

    kfilter.load_keywords()
    kfilter.prune_data()
    kfilter.write_output()
