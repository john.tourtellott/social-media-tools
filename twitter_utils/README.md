# Twitter Utils

Note that some of these scripts require geojson, geopy, and/or numpy.
On turtleland2, you can get these in the conda env name "minerva-dev"


## Usage
To generated output geojson, you must provide as input a geojson file
with the geometry for each of the US states. The file
shapefile_50_states.geojson is included in the shared folder, however,
you can substitute any other geojson file with one feature per state,
and that includes a "name" property with each feature.
